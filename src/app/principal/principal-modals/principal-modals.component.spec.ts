import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrincipalModalsComponent } from './principal-modals.component';

describe('PrincipalModalsComponent', () => {
  let component: PrincipalModalsComponent;
  let fixture: ComponentFixture<PrincipalModalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrincipalModalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrincipalModalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
