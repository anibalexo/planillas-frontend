import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Planilla } from '../models/planilla';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';

@Component({
  selector: 'app-principal-modals',
  templateUrl: './principal-modals.component.html',
  styleUrls: ['./principal-modals.component.css']
})
export class PrincipalModalsComponent implements OnInit {

  @Input() modalTitle;
  @Input() planilla: Planilla;  

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
  }

  descargarPdf() {
    const doc = new jsPDF();
    doc.addImage("assets/logo-letras-small.jpg", "JPEG", 80, 20, 50, 15);
    doc.setFontSize(17);
    doc.text("COMPAÑIA DE ECONOMIA MIXTA LOJAGAS", 45, 45);
    doc.setFontSize(10);
    doc.setTextColor(150);
    doc.text("Calle Valencia entre Burgos y Av. Turunuma. Barrio Turunuma Alto. Telf: 072614614, RUC: 1190067927001", 20,55)    
    doc.setDrawColor('gray');
    doc.line(14, 60, 196, 60);    
    autoTable(doc, { html: '#tableInfo', useCss: true, startY: 63, styles: {overflow: 'linebreak', textColor: 'gray'}})
    doc.setDrawColor('gray');
    doc.line(14, 105, 196, 105);
    autoTable(doc, { html: '#tableData', useCss: true, startY: 110, styles: {overflow: 'linebreak', textColor: 'gray'}})
    doc.setTextColor('red');
    doc.text("* DOCUMENTO SIN VALOR COMERCIAL", 14, 142);
    doc.setFontSize(10);
    doc.setTextColor('gray');
    doc.text("Este documento no es válido como factura, comprobante de pago, ni como sustento para crédito tributario. C.E.M. LOJAGAS no se responsabiliza por el mal uso que se le pudiera dar a esta información y se reserva el derecho de cambios en los valores por nuevos débitos y creditos que puedan alterar el TOTAL a pagar.", 
      14, 149, {align:'justify', maxWidth:180});
    doc.save("documento.pdf");
  }
}
