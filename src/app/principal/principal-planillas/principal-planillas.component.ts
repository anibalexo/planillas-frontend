import { Component, OnInit, Input } from '@angular/core';
import { Planilla } from '../models/planilla';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PrincipalModalsComponent } from '../principal-modals/principal-modals.component';

@Component({
  selector: 'app-principal-planillas',
  templateUrl: './principal-planillas.component.html',
  styleUrls: ['./principal-planillas.component.css']
})
export class PrincipalPlanillasComponent implements OnInit {

  @Input() planillas: Array<Planilla>;  

  constructor(
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
  }

  getTotal(planillas: Array<Planilla>): number {    
    let totalPlanillas = 0;
    planillas.forEach( doc => {
      totalPlanillas += doc.total;
    });
    return totalPlanillas;
  }

  openModal(doc: Planilla) {
    const modalRef = this.modalService.open(PrincipalModalsComponent, {size: 'lg'});
    modalRef.componentInstance.planilla = doc;
  }    

}
