import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AppService} from "../app.service";
import { Residencia, Planilla } from './models/planilla';
import { ToastService } from './services/toast.service';


@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  residencia: Residencia;
  listaPlanillas: Array<Planilla>;
  estadoBtnSubmit: Boolean = true; // en prod true
  showNewSearch: Boolean = true;  
  siteKey: string = '6LeQT_0UAAAAACvLxJOS1DT8Vk347iE621klbjPF';  
  searchForm = new FormGroup({
    txtCriterio: new FormControl(''),
  });

  constructor(
    public fb: FormBuilder, 
    public toastService: ToastService,
    private service: AppService
    ) {
    this.listaPlanillas = [];
   }

  ngOnInit(): void {
    this.searchForm = this.fb.group({
      txtCriterio: ['', [Validators.required, Validators.maxLength(13)]],
      recaptchaReactive: []
    });
  }

  async resolved(captchaResponse: string) {
    await this.sendTokenToBackend(captchaResponse);
  }

  sendTokenToBackend(token): void {
    this.service.sendToken(token).subscribe(
      data => {
        if(data['success']){
          this.estadoBtnSubmit = false;
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  obtenerPlanillas(criterio: string): void {    
    this.service.getPlanillas(criterio).subscribe(
      data => {
        if(data){
          this.residencia = data.residencia;
          this.listaPlanillas = data.documentos;
        }
      }, (error: any) => {
        console.log(error);
      }
    );
  }

  resetForm() {
    this.searchForm.reset();
    this.estadoBtnSubmit = true;
    this.showNewSearch = true;
    this.residencia = null;
    this.listaPlanillas = [];
  }

  onSubmit(form: FormGroup): void {    
    if(form.valid){
      this.obtenerPlanillas(form.controls.txtCriterio.value);
      this.showNewSearch = false;      
    } else {
      this.toastService.show('No se ha ingresado cédula, ruc o codigo bancario del cliente', {
        classname: 'bg-danger text-light', 
        delay: 4000, 
        autohide: true, 
        headertext: 'Atención'
      });
    }
  }

}
