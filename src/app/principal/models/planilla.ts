
export class Planilla {
    tipo_documento?: string;
    codigo_documento?: number;
    codigo_cliente?: number;
    dni_cliente?: string;
    nombre_cliente?: string;
    beneficiario?: string;
    numero_medidor?: string;
    departamento?: string;
    tipo_pago?: string;
    codigo_pago?: string;
    numero_factura?: string;
    fecha_documento?: string;
    fecha_vencimiento?: string;
    dias_plazo?: number;
    base?: number;
    iva?: number;
    total?: number;
    valor?: number;
}

export class Residencia {
    codigo?: number;
    nombre?: string;
    direccion?: string;
    contacto?: string;
    telefono?: string;
}