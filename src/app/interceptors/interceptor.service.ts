import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { ToastService } from '../principal/services/toast.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Injectable()
export class InterceptorService implements HttpInterceptor{

  constructor(
    public toastService: ToastService,
    private ngxService: NgxUiLoaderService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.ngxService.start();
    return next.handle(req).pipe(
      
      catchError( (err: any) => {
        this.ngxService.stop();
        if(err instanceof HttpErrorResponse ) {
          if (!navigator.onLine) {
            this.toastService.show('Intenta conectarte a una red', {
              classname: 'bg-danger text-light',
              delay: 5000,
              autohide: true,
              headertext: 'Notificación'
            });
          }
          if (err.status >= 400 && err.status < 500) {
            try {
              this.toastService.show(err.error.error, {
                classname: 'bg-danger text-light',
                delay: 5000,
                autohide: true,
                headertext: 'Notificación'
              });
            } catch (e) {
              this.toastService.show('Error notificación', {
                classname: 'bg-danger text-light',
                delay: 5000,
                autohide: true,
                headertext: 'Atención'
              });
            }
          } else {
            this.toastService.show('Ha ocurrido un error, intente mas tarde', {
              classname: 'bg-danger text-light',
              delay: 5000,
              autohide: true,
              headertext: 'Notificación'
            });            
          }

        }
        return of(err);
      }),
      finalize( () => this.ngxService.stop())
    );
  }

}


