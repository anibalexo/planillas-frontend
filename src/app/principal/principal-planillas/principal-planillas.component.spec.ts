import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrincipalPlanillasComponent } from './principal-planillas.component';

describe('PrincipalPlanillasComponent', () => {
  let component: PrincipalPlanillasComponent;
  let fixture: ComponentFixture<PrincipalPlanillasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrincipalPlanillasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrincipalPlanillasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
