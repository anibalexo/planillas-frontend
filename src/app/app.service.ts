import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ToastService } from './principal/services/toast.service';

@Injectable({ providedIn: 'root' })
export class AppService {

  private headers: HttpHeaders;
  //private urlApi: string = 'http://127.0.0.1:8001/';
  //private urlApi: string = 'http://192.168.1.64:8002/';
  private urlApi: string = 'https://api.lojagas.com.ec:8443/';

  constructor(private http: HttpClient, public toastService: ToastService) { }

  sendToken(tokenResponse: any) {
    const urlCatpcha = `${this.urlApi}api/auth/captcha/`;
    const body = {response: tokenResponse};
    this.headers = new HttpHeaders().set('Content-Type', 'application/json');
    const options = {headers: this.headers};
    return this.http
      .post<any>(urlCatpcha, body, options)
      .pipe(
        map( resp => resp )
      );
  }

  getPlanillas(criterio: string) {
    const urlPlanillas = `${this.urlApi}api/planillas/${criterio}`;
    this.headers = new HttpHeaders().set('Content-Type', 'application/json');
    const options = {headers: this.headers};
    return this.http
      .get(urlPlanillas, options)
      .pipe(
        map( (response: any) => {
          return response;
        }),
      );
  }

}

