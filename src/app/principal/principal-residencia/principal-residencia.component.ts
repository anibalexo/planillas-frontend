import { Component, OnInit, Input } from '@angular/core';
import { Residencia } from '../models/planilla';

@Component({
  selector: 'app-principal-residencia',
  templateUrl: './principal-residencia.component.html',
  styleUrls: ['./principal-residencia.component.css']
})
export class PrincipalResidenciaComponent implements OnInit {

  @Input() model: Residencia;
  constructor() { }

  ngOnInit(): void {
    
  }
  

}
