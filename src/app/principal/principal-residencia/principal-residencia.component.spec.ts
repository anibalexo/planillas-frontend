import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrincipalResidenciaComponent } from './principal-residencia.component';

describe('PrincipalResidenciaComponent', () => {
  let component: PrincipalResidenciaComponent;
  let fixture: ComponentFixture<PrincipalResidenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrincipalResidenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrincipalResidenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
