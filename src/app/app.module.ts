import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FormasPagoComponent } from './formas-pago/formas-pago.component';
import { PrincipalComponent } from './principal/principal.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { PrincipalResidenciaComponent } from './principal/principal-residencia/principal-residencia.component';
import { PrincipalPlanillasComponent } from './principal/principal-planillas/principal-planillas.component';
import { ToastsComponent } from './toasts/toasts.component';
import { InterceptorService } from './interceptors/interceptor.service';
import { ToastService } from './principal/services/toast.service';
import { PrincipalModalsComponent } from './principal/principal-modals/principal-modals.component';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { InformacionComponent } from './informacion/informacion.component';

@NgModule({
  declarations: [
    AppComponent,
    FormasPagoComponent,
    PrincipalComponent,
    NavbarComponent,
    PrincipalResidenciaComponent,
    PrincipalPlanillasComponent,
    ToastsComponent,
    PrincipalModalsComponent,
    InformacionComponent,    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RecaptchaModule,  //this is the recaptcha main module
    RecaptchaFormsModule, //this is the module for form incase form validation        
    HttpClientModule,
    NgbModule,
    NgxUiLoaderModule
  ],
  entryComponents:[
    PrincipalModalsComponent
  ],
  providers: [   
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
